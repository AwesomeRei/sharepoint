package sharepoint

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"strings"
)

type Auth struct {
	Url          string
	ClientID     string
	ClientSecret string
	Scope        string
}

type AuthResponse struct {
	TokenType    string
	ExpiresIn    float64
	ExtExpiresIn float64
	AccessToken  string
}

type Sharepoint interface {
	GetAuthToken() (AuthResponse, error)
	UploadDocument(site string, path string, fileName string, in *bytes.Reader) ([]byte, error)
	Search(site string, keyword string, fields []string, in *bytes.Reader) (SharepointSearchResult, error)
}

type SharepointClient struct {
	AuthConfig Auth
	Token      AuthResponse
}

type SharepointSearchResult struct {
	Value []SpSearchItems `json:"value"`
}
type SpSearchItems struct {
	CreatedDateTime      string     `json:"createdDateTime"`
	ID                   string     `json:"id"`
	LastModifiedDateTime string     `json:"lastModifiedDateTime"`
	Filename             string     `json:"name"`
	WebURL               string     `json:"weburl"`
	Size                 int        `json:"size"`
	CreatedBy            SpUser     `json:"createdBy"`
	LastModifiedBy       SpUser     `json:"lastModifiedBy"`
	File                 SpMimeType `json:"file"`
}
type SpUser struct {
	User SpUserDetail `json:"user"`
}
type SpUserDetail struct {
	Email       string `json:"email"`
	DisplayName string `json:"displayName"`
}

type SpMimeType struct {
	MimeType string `json:"mimeType"`
}

func NewSharepoint(authConfig Auth) Sharepoint {
	return &SharepointClient{
		AuthConfig: authConfig,
	}
}

func (c *SharepointClient) GetAuthToken() (AuthResponse, error) {
	method := "POST"

	payload := &bytes.Buffer{}
	writer := multipart.NewWriter(payload)
	_ = writer.WriteField("grant_type", "client_credentials")
	_ = writer.WriteField("client_secret", c.AuthConfig.ClientSecret)
	_ = writer.WriteField("client_id", c.AuthConfig.ClientID)
	_ = writer.WriteField("scope", c.AuthConfig.Scope)
	err := writer.Close()
	if err != nil {
		return AuthResponse{}, err
	}

	client := &http.Client{}
	req, err := http.NewRequest(method, c.AuthConfig.Url, payload)

	if err != nil {
		fmt.Println(err)
		return AuthResponse{}, err
	}

	req.Header.Set("Content-Type", writer.FormDataContentType())
	res, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return AuthResponse{}, err
	}
	defer res.Body.Close()

	var e map[string]interface{}
	if err := json.NewDecoder(res.Body).Decode(&e); err != nil {
		return AuthResponse{}, err
	}
	token := AuthResponse{AccessToken: e["access_token"].(string),
		TokenType:    e["token_type"].(string),
		ExpiresIn:    e["expires_in"].(float64),
		ExtExpiresIn: e["ext_expires_in"].(float64),
	}
	c.Token = token
	return token, nil
}

func (c *SharepointClient) UploadDocument(site string, path string, fileName string, in *bytes.Reader) ([]byte, error) {
	url := fmt.Sprintf("https://graph.microsoft.com/v1.0/sites/%v/drive/root:/%v/%v:/content", site, path, fileName)
	method := "PUT"
	headers := make(map[string]string)
	headers["Content-Type"] = "multipart/form-data"

	return callhttp(url, method, c.Token.AccessToken, headers, in)
}

func (c *SharepointClient) Search(site string, keyword string, fields []string, in *bytes.Reader) (SharepointSearchResult, error) {
	selector := ""
	var ssr SharepointSearchResult
	if fields != nil {
		selector = fmt.Sprintf("?select=%v", strings.Join(fields, ","))
	}
	url := fmt.Sprintf("https://graph.microsoft.com/v1.0/sites/%v/drive/root/search(q='%v')%v", site, keyword, selector)
	method := "GET"

	payload, err := callhttp(url, method, c.Token.AccessToken, nil, in)
	if err != nil {
		fmt.Printf("error when search sharepoint : %v \n", err.Error())
		return ssr, err
	}

	err = json.Unmarshal(payload, &ssr)
	if err != nil {
		fmt.Printf("error when converting sharepoint result to json : %v \n", err.Error())
		return ssr, err

	}
	return ssr, err

}

//example usage
/*
    keyword := "search keyword"
    siteID := os.Getenv("SHAREPOINT_SITE_ID")
 	filter := ""
	drive := "b!eRQouaMnO0OJjNWAsJNZVcbMND1d0chPinNzSnT33teZaZLR-LKBS7_t6f6Rnwiu/items/01JK5X2M5AWMI5NN5GIRBLRKU56Y7QGVP4"
	SearchDocument(siteID, drive, keyword, nil, filtter, bytes.NewReader([]bytes("")))
*/
func (c *SharepointClient) SearchDocumentInFolder(site string, drive string, keyword string, fields []string, filter string, in *bytes.Reader) (SharepointSearchResult, error) {
	if c.Token.AccessToken == "" {
		c.GetAuthToken()
	}

	selector := fmt.Sprintf("?$top=%v", 10)
	var ssr SharepointSearchResult
	if fields != nil {
		selector = fmt.Sprintf("&select=%v", strings.Join(fields, ","))
	}
	if filter != "" {
		selector = fmt.Sprintf("%v&%v", selector, filter)
	}
	url := fmt.Sprintf("https://graph.microsoft.com/v1.0/sites/%v/drives/%v/search(q='%v')%v", site, drive, keyword, selector)
	fmt.Printf("[INFO] Searching to URL=%v \n", url)
	method := "GET"

	payload, err := callhttp(url, method, c.Token.AccessToken, nil, in)
	if err != nil {
		fmt.Printf("[ERROR] error when search sharepoint : %v \n", err.Error())
		return ssr, err
	}

	err = json.Unmarshal(payload, &ssr)
	if err != nil {
		fmt.Printf("[ERROR] error when converting sharepoint result to json : %v \n", err.Error())
		return ssr, err

	}
	return ssr, err

}

func callhttp(url string, method string, token string, headers map[string]string, in *bytes.Reader) ([]byte, error) {
	client := &http.Client{}
	req, err := http.NewRequest(method, url, in)

	if err != nil {
		fmt.Println(err)
		return nil, nil
	}

	req.Header.Add("Authorization", fmt.Sprintf("Bearer %v", token))
	for k, v := range headers {
		req.Header.Add(k, v)
	}
	res, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	return body, nil
}
