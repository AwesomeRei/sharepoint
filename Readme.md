# Sharepoint Client
Sharepoint Client to connect to Sharepoint using clientID and client Secret

## Example
```go
package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	sh "gitlab.com/AwesomeRei/sharepoint"
)

func main() {
	site := "site-id"
	path := "path"
	fileName := "filename.ext"
	auth := sh.Auth{
		Url:          "sharepoint authentication url",
		ClientID:     "client-id",
		ClientSecret: "client-secret",
		Scope:        "https://graph.microsoft.com/.default",
	}
	cli := sh.NewSharepoint(auth)
	_, err := cli.GetAuthToken()
	if err != nil {
		fmt.Errorf(err.Error())
	}

	response, err := http.Get("some_image_url")
	if err != nil {
		fmt.Print(err.Error())
		os.Exit(1)
	}

	responseData, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Fatal(err)
	}

	resp, err := cli.UploadDocument(site, path, fileName, bytes.NewReader(responseData))
	if err != nil {
		fmt.Errorf(err.Error())
	}
	fmt.Println(string(resp))

}

```